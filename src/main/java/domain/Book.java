package domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book {

    private int id;
    private String title;
    private String description;
    private int authorId;
    private long year;

    private String authorsName;

    public Book(int id, String title, String description, int authorId, long year) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.authorId = authorId;
        this.year = year;
    }
}