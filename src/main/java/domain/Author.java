package domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class Author {

    private int id;
    private String name;
    private List<Integer> bookIds;


    public Author(int id, String name, int... bookIds) {
        this.id = id;
        this.name = name;
        this.bookIds = Arrays.stream(bookIds).boxed().collect(Collectors.toList());
    }
}
