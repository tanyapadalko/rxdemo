package reactiveEventManager;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import web.rest.requestModel.BaseRequestModel;
import web.rest.responseModel.BaseResponseModel;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseEventManager<ReqM extends BaseRequestModel, ResM extends BaseResponseModel>
        implements Observer<ResM> {

    private CompositeDisposable disposables = new CompositeDisposable();
    private Subject<ReqM> events = BehaviorSubject.create();
    Map<String, Object> viewModel = new HashMap<>();


    public void event(ReqM event) {
        events.onNext(event);
        events.flatMap(this::onEvent).blockingSubscribe(this);
    }

    @Override
    public void onSubscribe(Disposable disposable) {
        disposables.add(disposable);
    }

    @Override
    public void onNext(ResM responseModel) {
        viewModel.put(responseModel.getClass().getSimpleName(), (responseModel));
        onClear();
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
        System.out.print(throwable.getMessage());
        disposables.clear();
        events.flatMap(this::onEvent).subscribe(this);
    }

    @Override
    public void onComplete() { /*ignored*/ }

    private void onClear() {
        disposables.dispose();
    }

    protected abstract Observable<ResM> onEvent(ReqM requestModel);

    public Map<String, Object> getViewModel() {
        return viewModel;
    }
}

