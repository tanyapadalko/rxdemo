package web.rest.controller;

import reactiveEventManager.BookEventManager;
import spark.ModelAndView;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import web.rest.requestModel.BookRequestModel;

import static spark.Spark.get;

public class BookController {

    public BookController(final BookEventManager bookEventManager) {

        get("/books", (req, res) -> {
            bookEventManager.event(new BookRequestModel.All(null));
            return new ModelAndView(bookEventManager.getViewModel(), "books");
        }, new ThymeleafTemplateEngine());

    }
}
