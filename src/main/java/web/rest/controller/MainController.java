package web.rest.controller;

import reactiveEventManager.MainEventManager;
import spark.ModelAndView;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import web.rest.requestModel.MainRequestModel;
import static spark.Spark.get;


public class MainController {

    public MainController(final MainEventManager mainEventManager) {

        get("/all", (req, res) -> {
            mainEventManager.event(new MainRequestModel.All());
            return new ModelAndView(mainEventManager.getViewModel(), "all");
        }, new ThymeleafTemplateEngine());
    }
}
