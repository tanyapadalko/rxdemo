package web.rest.responseModel;

import domain.Author;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AuthorResponseModel extends BaseResponseModel {

    @Getter
    @Setter
    public class One extends AuthorResponseModel {

        public Author author;
    }

    @Getter
    @Setter
    public class All extends AuthorResponseModel {

        public List<Author> authors;
    }
}
