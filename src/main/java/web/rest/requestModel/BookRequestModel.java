package web.rest.requestModel;

import lombok.Getter;

@Getter
public class BookRequestModel extends BaseRequestModel {
    public enum EventType {
        ById, All, ByAuthorId
    }
    public EventType eventType;

    @Getter
    public static class ById extends BookRequestModel {
        public int id;

        public ById(int id) {
            this.id = id;
            eventType = EventType.ById;
        }
    }

    @Getter
    public static class All extends BookRequestModel {
        public String title;

        public All(String title) {
            this.title = title;
            eventType = EventType.All;
        }
    }

    @Getter
    public class ByAuthorId extends BookRequestModel {
        public int authorId;

        public ByAuthorId(int authorId) {
            this.authorId = authorId;
            eventType = EventType.ByAuthorId;
        }

    }
}
