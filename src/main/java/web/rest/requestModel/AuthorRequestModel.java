package web.rest.requestModel;

import lombok.Getter;

@Getter
public class AuthorRequestModel extends BaseRequestModel {
    public enum EventType {
        ById, All
    }
    public EventType eventType;

    @Getter
    public class ById extends AuthorRequestModel {
        public int id;

        public ById(int id) {
            this.id = id;
            eventType = EventType.ById;
        }
    }

    @Getter
    public class All extends AuthorRequestModel {
        public String name;

        public All(String name) {
            this.name = name;
            eventType = EventType.All;
        }
    }
}
