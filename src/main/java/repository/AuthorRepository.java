package repository;

import domain.Author;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AuthorRepository {

    public Observable<List<Author>> findAll(String name) {
        return Observable.fromCallable(() -> {
            if (name == null)
                return populateDummyAuthors();
            return populateDummyAuthors().stream().filter(a -> a.getName().contains(name)).collect(Collectors.toList());
        }).subscribeOn(Schedulers.io());
    }

    public Observable<Author> findById(int id) {
        return Observable.fromCallable(() -> populateDummyAuthors().stream()
                .filter(a -> a.getId() == id).findAny().get());
    }


    private List<Author> populateDummyAuthors() {
        List<Author> authors = new ArrayList<>();
        authors.add(new Author(1, "Emily Bronte", 1));
        authors.add(new Author(2, "Antoine de Saint-Exupery", 2));
        authors.add(new Author(3, "George Orwell", 3, 11));
        authors.add(new Author(4, "Mary Wollstonecraft Shelley", 4));
        authors.add(new Author(5, "Oscar Wilde", 5));
        authors.add(new Author(6, "Charles Dickens", 6, 7));
        authors.add(new Author(7, "Howard Phillips Lovecraft", 8, 9, 10));

        return authors;
    }
}
