package repository;

import domain.Book;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BookRepository {

    public Observable<List<Book>> findAll(String title) {
        return Observable.fromCallable(() -> {
            List<Book> books;
            if (title == null)
                books = populateDummyBooks();
            else
                books = populateDummyBooks().stream().filter(b -> b.getTitle().contains(title)).collect(Collectors.toList());

            return books;
        }).subscribeOn(Schedulers.io());
    }

    public Observable<Book> findById(int id) {
        return Observable.fromCallable(() -> populateDummyBooks().stream().filter(b -> b.getId() == id).findAny().get());
    }

    public Observable<List<Book>> findAllByAuthorId(int authorId) {
        return Observable.fromCallable(() ->
                populateDummyBooks().stream().filter(b -> b.getAuthorId() == authorId).collect(Collectors.toList()));
    }

    public List<Book> findAllByIdIn(List<Integer> ids) {
        return populateDummyBooks().stream().filter(b -> ids.contains(b.getId())).collect(Collectors.toList());
    }


    private List<Book> populateDummyBooks() {
        List<Book> books = new ArrayList<>();
        books.add(new Book(1, "Wuthering Heights", "This best-selling Norton Critical Edition is based on the 1847 first edition of the novel.", 1, 1847));
        books.add(new Book(2, "The Little Prince", "Moral allegory and spiritual autobiography", 2, 1943));
        books.add(new Book(3, "Animal Farm", "A farm is taken over by its overworked, mistreated animals. With flaming idealism and stirring slogans, they set out to create a paradise of progress, justice, and equality.", 3, 1984));
        books.add(new Book(4, "Frankenstein", "At once a Gothic thriller, a passionate romance, and a cautionary tale about the dangers of science, Frankenstein tells the story of committed science student Victor Frankenstein. ", 4, 1818));
        books.add(new Book(5, "The Picture of Dorian Gray", "A story of a fashionable young man who sells his soul for eternal youth and beauty", 5, 1890));
        books.add(new Book(6, "Great Expectations", "orphaned Pip is apprenticed to the dirty work of the forge but dares to dream of becoming a gentleman — and one day, under sudden and enigmatic circumstances, he finds himself in possession of \"great expectations.\"", 6, 1861));
        books.add(new Book(7, "A Christmas Carol", "To bitter, miserly Ebenezer Scrooge, Christmas is just another day. But all that changes when the ghost of his long-dead business partner appears, warning Scrooge to change his ways before it's too late.", 6, 1843));
        books.add(new Book(8, "The Call of Cthulhu ", "One of the feature stories of the Cthulhu Mythos, H.P. Lovecraft's 'the Call of Cthulhu' is a harrowing tale of the weakness of the human mind when confronted by powers and intelligences from beyond our world.", 7, 1928));
        books.add(new Book(9, "At the Mountains of Madness", "Deliberately told and increasingly chilling recollection of an Antarctic expedition's uncanny discoveries --and their encounter with an untold menace in the ruins of a lost civilization--is a milestone of macabre literature.", 7, 1936));
        books.add(new Book(10, "Necronomicon: The Best Weird Tales", "This tome presents original versions of many of his most harrowing stories, including the complete Cthulhu Mythos cycle, in order of publication.", 7, 2008));
        books.add(new Book(11, "Nineteen Eighty-Four", "The book offers political satirist George Orwell's nightmarish vision of a totalitarian, bureaucratic world and one poor stiff's attempt to find individuality.", 3, 1949));

        return books;
    }
}
