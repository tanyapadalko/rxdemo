import reactiveEventManager.BookEventManager;
import reactiveEventManager.MainEventManager;
import repository.AuthorRepository;
import repository.BookRepository;
import service.BookService;
import service.MainService;
import web.rest.controller.BookController;
import web.rest.controller.MainController;

public class Main {

    public static void main(String[] args) {

        BookRepository bookRepository = new BookRepository();
        AuthorRepository authorRepository = new AuthorRepository();

        BookService bookService = new BookService(bookRepository, authorRepository);
        MainService mainService = new MainService(bookRepository, authorRepository);

        BookEventManager bookEventManager = new BookEventManager(bookService);
        MainEventManager mainEventManager = new MainEventManager(mainService);


        new BookController(bookEventManager);
        new MainController(mainEventManager);
    }
}
