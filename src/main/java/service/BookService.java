package service;

import io.reactivex.Observable;
import repository.AuthorRepository;
import repository.BookRepository;
import web.rest.requestModel.BookRequestModel;
import web.rest.responseModel.BookResponseModel;

public class BookService {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    public BookService(final BookRepository bookRepository, final AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    public Observable<BookResponseModel> handleAll(BookRequestModel.All requestModel) {
        return bookRepository.findAll(requestModel.getTitle())
                .flatMapIterable(books1 -> books1)
                .flatMap(book ->
                        authorRepository.findById(book.getAuthorId()).map(author -> {
                            book.setAuthorsName(author.getName());
                            return book;
                        })).toList().toObservable()
                .map(books -> {
                    BookResponseModel.All all = new BookResponseModel.All();
                    all.setBooks(books);
                    return all;
                });
    }

    public Observable<BookResponseModel> handleById(BookRequestModel.ById requestModel) {
        return bookRepository.findById(requestModel.getId()).map(book -> {
            BookResponseModel.One one = new BookResponseModel.One();
            one.setBook(book);
            return one;
        });
    }

    public Observable<BookResponseModel> handleAllByAuthorId(BookRequestModel.ByAuthorId requestModel) {
        return bookRepository.findAllByAuthorId(requestModel.getAuthorId()).map(books -> {
            BookResponseModel.All all = new BookResponseModel.All();
            all.setBooks(books);
            return all;
        });
    }
}
