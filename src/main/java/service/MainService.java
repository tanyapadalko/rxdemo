package service;

import io.reactivex.Observable;
import repository.AuthorRepository;
import repository.BookRepository;
import web.rest.responseModel.MainResponseModel;

public class MainService {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    public MainService(final BookRepository bookRepository, final AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }


    public Observable<MainResponseModel> getAll() {
        return Observable.zip(bookRepository.findAll(null),
                authorRepository.findAll(null),
                MainResponseModel.All::new);
    }
}