# rxdemo

Reactive eXtension (http://reactivex.io. a.k.a Rx) is an implementation of the reactive programming principles to 
“compose asynchronous and event-based programs by using observable sequence.”

###Why Rx?

Reactive Extensions are about sequences. With sequences we can create projections, transformations and filters.
Reactive Extensions can get pretty complex and is not always intuitive, but you can create some elegant solutions with it.


This architecture is an implementation of MVI (Event based) with MVVM (EventManager contains ModelAndView map) and 
clean architecture for connecting RxJava chains.MVI is the most useful case for Rx because Rx is an event based 
reactive library and by combining with defined input(*X*RequestModel) and output(*X*ResponseModel) models 
you get centralized stream manager(*X*EventManager).

##Project Structure

`Models`

There are two types of models:
`RequestModels`(Event model sent from View to EventManager through Controller) 
`ResponseModels` (Result model with collected data required for UI/View)

Each of these models has its own nested classes which contains only required data for exact event. 
In this way you restrict to use only the data needed for particular task. By using one class which 
contains every possible field for a block (in my example which are Book and Author MVCs) you get not 
completely filled model and you can simply try to get not existing data.

`ReactiveEventManager`

EventManager is responsible for detecting received events proceeding to services to create stream of requests.
BaseEventManager.event(RequestModel) is a method called from Controller which triggers the chain in itself by calling 
Subject.onNext() with received data. Subject is something like a proxy, which is Observer and Observable at the same time.

_Note: blockingSubscribe is used only because of an imperative nature of REST applications where data should be returned 
in a controller method from which it was called._

`Service`

Service is just a "helper" for EventManager, which contains an implementation of methods per event. Also it is 
responsible for all your data processing.

`Repository`

Populates dummy data for demo purposes, in real life application it can be simply a JPA Repository, etc.